angular.module('myApp', ['ui.router'])
    .config(["$stateProvider",  function ($stateProvider) {
        $stateProvider
            .state("index", {
                url: '/index',
                views:{
                    'header':{template:"<div>头部内容</div>"},
                    'nav':{template:"<div>菜单内容</div>"},
                    'body':{template:"<div>展示内容</div>"}
                }
        })

}]);
