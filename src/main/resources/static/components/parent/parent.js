angular.module('myApp', ['ui.router'])
    .config(["$stateProvider",  function ($stateProvider) {
        $stateProvider
            .state("parent", {//父路由
                url: '/parent',
                template:'<div>parent'
                    +'<div ui-view><div>'// 子View
                    +'</div>'
            })
            .state("parent.child", {//子路由
                url: '/child',
                template:'<div>child</div>'
            })
}]);
