angular.module('myApp',['ui.router'])

    .config(['$stateProvider',function ($stateProvider) {
        $stateProvider
            .state('index',{
                url:'/index',
                views:{
                    'index':{template:"<div><div ui-view='header'></div><div ui-view='nav'></div> <div ui-view='body'></div></div>"},
                    //这里必须要绝对定位
                    'header@index':{template:'<div>头部内容header</div>'},
                    'nav@index':{template:'<div>菜单内容nav</div>'},
                    'body@index':{template:'<div>展示内容contents</div>'}
                }
            })
            //绝对定位
            .state('index.content1',{
                url:'content1',
                views:{
                    'body@index':{template:'<div>content11111111111</div>'}
                }
            })
            //相对定位：该状态里的body的ui-view为相对路径下的（即没有说明具体是哪个模板）
            .state('index.content2',{
                url:'/content2',
                views:{
                    'body':{template:'<div>content2222222222222222222222</div>'}
                }
            })
    }])