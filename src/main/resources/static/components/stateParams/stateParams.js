angular.module('myApp', ['ui.router'])
    .config(["$stateProvider",  function ($stateProvider) {
        $stateProvider
            .state("index", {
                url: '/index/:id',
                template:"<div>indexcontent</div>",
                controller:function($stateParams){
                    alert($stateParams.id)
                }
            })
            .state("test", {
                url: '/test/:username',
                template:"<div>testContent</div>",
                controller:function($stateParams){
                    alert($stateParams.username)
                }
            })

    }]);
