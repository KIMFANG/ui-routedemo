angular.module('helloworld', ['ui.router'])

    .config(function($stateProvider){
        $stateProvider.state('hello',{
            url:'/hello',
            template:'<h3>hello world!</h3>'
        }).state('world',{
            url:'/world',
            templateUrl:'../resources/static/components/world/world.html'
        }).state('world.world1',{
            url:'/world/world-1',
            template:'<h3>This is a World 1</h3>'
        }).state('world2',{
            url:'/world/world-2',
            template:'<h3>world2并不依赖于world，在我们点击它的时候，他会替换掉index.html中的ui view</h3>'
        })

    });


